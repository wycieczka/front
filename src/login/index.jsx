import './css/index.css'
import styles from './sass/index.module.sass';
import Form from './components/form';
import { useParams } from 'react-router';
import { useEffect, useState } from 'react';

export const Login = () => {
  const [data, setData] = useState(null);
  const { trip_id } = useParams();
  useEffect(() => {
    (async () => {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/trips/${trip_id}`, {
        metehod: 'GET',
        headers: {
          'Accept': 'application/json'
        },
      });
      const data = await response.json();
      console.log(data);
      setData(data)
    })();
  }, []);
  return (
    <div className={styles.root}>
      <main>
        <div className=".container">
          <Form />
        </div>
      </main>
    </div>
  );
};
export default Login;