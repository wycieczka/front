import { useState } from "react";

export const Form = () => {
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState("");
  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      email: e.target.email.value,
      password: e.target.password.value,
    };
    console.log(JSON.stringify(data));
    setLoading(true);
    setError(false);

    const response = await fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    console.log(response);
    setLoading(false);
    if (response.ok) {
      setSuccess(true);
    } else {
      console.log("error");
      setError("Wrong login password");
    }
  };
  if (success) {
    return (
      <div>
        <div class="title"> Succesful login</div>
        <a href="../">
          <button>
            Go to main page
          </button>
        </a>
      </div>
    )
  }
  return (
    <form action="#" onSubmit={handleSubmit}>
      <div class="title">Register</div>
      <div className="user-details">
        <div className="input-box">
          <span className="details">Email</span>
          <input id="email" name="email" type="text" placeholder="Enter your email" required/>
        </div>
        <div className="input-box">
          <span className="details">Password</span>
          <input id="password" name="password" type="password" placeholder="Enter your password" required/>
        </div>
      </div>
      
      <div className="button">
        <input type="submit" value="Log in"/>
      </div>

      { loading && <div>procesowanie rejestracji...</div>}
      { error &&
        <div className="message">{error}</div>
      }
    </form>
  );
}
export default Form;