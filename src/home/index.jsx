import { useEffect } from "react";

import Hero from './components/hero';
import Main from './components/main';

const Home = () => {
  return (
    <>
      <Hero />
      <Main />
    </>
  );
};
export default Home;