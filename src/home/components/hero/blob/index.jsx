import styles from './sass/index.module.sass';

export const Blob = () => {
  return (
    <>
      <div className={styles.root}>
        <svg viewBox="0 0 1390 235" fill="none">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M834 54.0006C1010 116.001 1102 -66 1308 28.0004C1379.9 60.8092 1390 122.001 1390 234.295C1260 234.295 415.995 234.295 327.24 234.295C234 234.295 150 234.294 0.000112144 234.295C3.19946e-05 150.001 0.000206603 184.001 0.000207226 108.001C240 -29.9992 245.279 157.775 433.64 64.8876C622 -27.9999 658 -8 834 54.0006Z" fill="#F3EDE2"/>
        </svg>
      </div>
    </>
  );
};
export default Blob;