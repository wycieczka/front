import styles from './sass/index.module.sass';
import Blob from './blob';

export const Hero = () => {
  return (
    <section className={styles.root}>
      <header>
        <a className={styles.logo} href='/'>
          Wycieczka
        </a>
        <a className={styles.userBtn} href='/register'>
          Sign up
        </a>
        <a className={styles.userBtn} href='/login'>
          Sign in
        </a>
      </header>
      <Blob />
    </section>
  );
}
export default Hero;