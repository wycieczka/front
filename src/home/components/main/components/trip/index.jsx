import styles from './sass/index.module.sass';

export const Trip = ({ id, name, description, Image }) => {
  console.log(Image);
  const thumbnail = Image ? Image.path : `https://img.freepik.com/darmowe-zdjecie/piekny-krajobraz-z-roslinnoscia-i-drzewami_1160-311.jpg?t=st=1656172624~exp=1656173224~hmac=ed7b9e3174fec64d3218488cb45296f82f0d1408e6124fe4801e0f560aefe645&w=1380`;
  return (
    <li key={id} className={styles.root}>
      <a href={`/trip/${id}`}>
        <img classsName={styles.thumbnail} src={thumbnail} />
        <span className={styles.title}>
          {name}
        </span>
      </a>
    </li>
  );
};
export default Trip;