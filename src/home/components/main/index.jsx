import { useEffect, useState } from 'react';
import styles from './sass/index.module.sass';

import Trip from './components/trip';

export const Main = () => {
  const [trips, setTrips] = useState([]);
  useEffect(() => {
    (async () => {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/trips`, {
        metehod: 'GET',
        headers: {
          'Accept': 'application/json'
        },
      });
      const trips = await response.json();
      console.log(trips);
      setTrips(trips)
    })();
  }, []);
  return (
    <main className={styles.root}>
      <h2>Our trips:</h2> 
      <ul className={styles.trips}>
        {trips.map((trip, i) => {
          console.log(trip.Image);
          return (
            <Trip key={i} {...trip} />
          );
        })}
      </ul>
    </main>
  );
}
export default Main;