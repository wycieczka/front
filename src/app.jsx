import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from './home';
import Trip from './trip';
import Register from './register';
import Login from './login';

import './global/sass/global.sass';
import { useEffect, useState } from "react";

const App = () => {
  // const [data, setData] = useState({});
  // useEffect(() => {
  //   console.log('>>');
  //   (async () => {
  //     const data = await fetch('http://0.0.0.0:8080/api/reservations', {
  //       method: 'post',
  //       headers: {
  //         'Content-Type': 'application/json'
  //       },
  //       body: JSON.stringify({
  //         first_name: 'Arnold',
  //         family_name: 'Citko',
  //         email: 'arnold69pl@gmail.pl',
  //         phone_number: '997',
  //         trip_id: 1,
  //         ticket_count: 4
  //       })
  //     });
  //     setData(data);
  //     console.log(data);
  //   })();
  // }, []);
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path='/register' element={<Register />} />
          <Route path='/login' element={<Login />} />
          <Route path='/trip/:trip_id' element={<Trip />} />
          <Route path='/' element={<Home />} />
        </Routes>
      </BrowserRouter>
      {/* const { ticket_count, email, first_name, phone_number, trip_id, user_id } = req.body; */}

      {/* <form method="post" action="http://0.0.0.0:8080/api/reservations">
        <input type="number" name="ticket_count" />
        <input type="email" name="email" />
        <input type="first_name" name="first_name" />
        <input type="phone_number" name="phone_number" />
        <input type="trip_id" name="trip_id" />
        <button>sadsad</button>
      </form> */}
      {/* {data} */}
      {/* <Wrapper>
        <Header />
        
      </Wrapper> */}
    </>
  );
};
export default App;