import { useState } from "react";

export const Form = () => {
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState("");
  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      first_name: e.target.first_name.value,
      family_name: e.target.family_name.value,
      email: e.target.email.value,
      phone_number: e.target.phone_number.value,
      password: e.target.password.value,
    };
    // console.log(JSON.stringify(data));
    setLoading(true);
    setError(false);
    const response = await fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    console.log(response);
    setLoading(false);
    if (response.ok) {
      setSuccess(true);
    } else {
      console.log("error");
      setError("Wrong registration");
    }
  };
  if (success) {
    return (
      <div>
        <div class="title"> Succesful registration</div>
        <a href="../">
          <button>
            Go to main page and log in
          </button>
        </a>
      </div>
    )
  }
  return (
    <form action="#" onSubmit={handleSubmit}>
      <div class="title">Register</div>
      <div className="user-details">
        <div className="input-box">
          <span className="details">First Name</span>
          <input id="first_name" type="text" name="first_name" placeholder="Enter your name" required/>
        </div>
        <div className="input-box">
          <span className="details">Family Name</span>
          <input id="family_name" type="text" placeholder="Enter your family name" required/>
        </div>
        <div className="input-box">
          <span className="details">Email</span>
          <input id="email" name="email" type="text" placeholder="Enter your email" required/>
        </div>
        <div className="input-box">
          <span className="details">Phone Number</span>
          <input id="phone_number" name="phone_number" type="text" placeholder="Enter your number" required/>
        </div>
        <div className="input-box">
          <span className="details">Password</span>
          <input id="password" name="password" type="password" placeholder="Create password" required/>
        </div>
      </div>
      
      <div className="button">
        <input type="submit" value="Register" disabled={loading} />
      </div>
      { loading && <div>procesowanie rejestracji...</div>}
      { error &&
        <div className="message">{error}</div>
      }
    </form>
  );
}
export default Form;