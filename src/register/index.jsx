import './css/index.css'
import styles from './sass/index.module.sass';
import Form from './components/form';
import { useParams } from 'react-router';
import { useEffect, useState } from 'react';

export const Register = () => {
  const [data, setData] = useState(null);
  return (
    <div className={styles.root}>
      <main>
        {/* <div className={styles.picture}></div> */}
        <div className=".container">
          
          <Form />
        </div>
      </main>
    </div>
  );
};
export default Register;